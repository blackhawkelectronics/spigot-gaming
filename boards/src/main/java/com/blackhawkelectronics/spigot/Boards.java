package com.blackhawkelectronics.spigot;

import com.blackhawkelectronics.spigot.application.listeners.PlayerListener;
import com.blackhawkelectronics.spigot.domain.State;
import com.blackhawkelectronics.spigot.infrastructure.commands.AdminCommand;
import com.blackhawkelectronics.spigot.infrastructure.commands.JoinGameCommand;
import com.blackhawkelectronics.spigot.infrastructure.commands.PlayerCommand;
import com.blackhawkelectronics.spigot.infrastructure.commands.Roll;
import com.blackhawkelectronics.spigot.application.listeners.AdminListener;
import com.blackhawkelectronics.spigot.application.listeners.GameListener;
import com.blackhawkelectronics.spigot.domain.game.Game;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Boards extends JavaPlugin {
    private final FileConfiguration config = getConfig();
    private final State state = new State();

    @Override
    public void onEnable() {
        config.addDefault("enabled", true);
        config.addDefault("world", "World");
        config.options().copyDefaults(true);
        saveConfig();

        getServer().getPluginManager().registerEvents(new AdminListener(config), this);

        loadGames();
        loadCommands();
        loadListeners();

        getLogger().info("Gaming plugin enabled. Ready to play!");
    }

    @Override
    public void onDisable(){
        // Need to save any data before shutting down!!!
        // For example, keeping track of high-scores or veterans

        getLogger().info("Gaming plugin disabled. Thanks for playing!");
    }

    private void loadGames(){
        // scan data folder for files ending in .yml but not config.yml, the primary config.
        // For each config file, read and pass to a new Game object
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }

        String path = getDataFolder().getAbsolutePath();
        List<String> gameList = config.getStringList("games");
        for(String gameName : gameList){
            state.addGame(new Game(gameName, path, getGameConfig(gameName)));
        }
    }

    private void loadCommands() {
        Map<String, CommandExecutor> commands = new HashMap<String, CommandExecutor>() {
            {
                put(Roll.name, new Roll());
                put(JoinGameCommand.name, new JoinGameCommand());
                put(AdminCommand.name, new AdminCommand());
                put(PlayerCommand.name, new PlayerCommand());
            }
        };
        for (Map.Entry<String, CommandExecutor> command : commands.entrySet()) {
            PluginCommand pluginCommand = getCommand(command.getKey());
            if(pluginCommand != null) {
                pluginCommand.setExecutor(command.getValue());
            } else {
                getLogger().warning("Unable to load command " + command.getKey());
            }
        }
    }

    private void loadListeners() {
        PluginManager manager = getServer().getPluginManager();
        manager.registerEvents(new AdminListener(config), this);
        manager.registerEvents(new GameListener(state), this);
        manager.registerEvents(new PlayerListener(state), this);
    }

    private FileConfiguration getGameConfig(String name) {
        // TODO: put migrations here in the future.
        File gameConfigFile = new File(getDataFolder(), name + ".yml");

        if(!gameConfigFile.exists()) {
            try {
                InputStream inputStream = getResource("gameDefault.yml");
                if(inputStream != null) {
                    OutputStream outputStream = new FileOutputStream(gameConfigFile);
                    inputStream.transferTo(outputStream);
                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();
                } else {
                    getLogger().warning("unable to make config file for " + name);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileConfiguration gameConfig = new YamlConfiguration();
        try {
            gameConfig.load(gameConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            getLogger().warning("Unable to load config file for " + name);
            e.printStackTrace();
        }

        return gameConfig;
    }
}