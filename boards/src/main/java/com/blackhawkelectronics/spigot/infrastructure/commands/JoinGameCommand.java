package com.blackhawkelectronics.spigot.infrastructure.commands;

import com.blackhawkelectronics.spigot.application.events.game.GameJoin;
import com.blackhawkelectronics.spigot.domain.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

import java.util.Map;

public class JoinGameCommand implements CommandExecutor {
    public final static String name = "joingame";
    private final PluginManager manager;

    public JoinGameCommand(){
        manager = Bukkit.getPluginManager();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if(sender instanceof Player) {
            if(args.length == 1) {
                manager.callEvent(new GameJoin((Player) sender, args[0]));
                return true;
            } else {
                sender.sendMessage("Failed to join game");
                return false;
            }

        } else {
            sender.sendMessage("The joingame command must be run by a player in a game!");
            return false;
        }
    }
}
