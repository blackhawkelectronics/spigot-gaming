package com.blackhawkelectronics.spigot.infrastructure.commands;

import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class PlayerCommand implements CommandExecutor {
    public final static String name = "boards";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args){
        if(sender instanceof Player) {
            Player player = (Player) sender;
        } else {
            sender.sendMessage(Color.RED + "This is a player command only!");
        }

        return true;
    }
}
