package com.blackhawkelectronics.spigot.infrastructure.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

public class Roll implements CommandExecutor {
    public final static String name = "roll";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player) sender;
            player.sendMessage("ROLL....");
            return true;
        } else {
            sender.sendMessage("The roll command must be run by a player in a game!");
            return false;
        }
    }
}
