package com.blackhawkelectronics.spigot.infrastructure.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AdminCommand implements CommandExecutor {
    public final static String name = "boardsadmin";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args){
        if(!sender.hasPermission("boards.admin")) {
            sender.sendMessage("You do not have permission to use that command");
            return false;
        }
        sender.sendMessage("Gaming mod admin!");
        return true;
    }
}
