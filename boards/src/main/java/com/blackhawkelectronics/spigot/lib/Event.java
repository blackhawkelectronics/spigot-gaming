package com.blackhawkelectronics.spigot.lib;

import org.bukkit.event.HandlerList;

public abstract class Event extends org.bukkit.event.Event {
    // REQUIRED for Spigot Events
    protected static final HandlerList HANDLERS = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
