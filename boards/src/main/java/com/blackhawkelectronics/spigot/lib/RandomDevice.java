package com.blackhawkelectronics.spigot.lib;

public interface RandomDevice {
    String getRandom();
}
