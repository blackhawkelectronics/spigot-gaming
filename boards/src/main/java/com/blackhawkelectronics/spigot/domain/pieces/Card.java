package com.blackhawkelectronics.spigot.domain.pieces;

import javax.annotation.Nullable;

public class Card {
    private final String name;
    private final String value;
    private final boolean isPrivate;

    public Card(String cardName, @Nullable String cardValue, boolean isPrivateCard) {
        name = cardName;
        value = cardValue;
        isPrivate = isPrivateCard;
    }

    public String getName() {
        return name;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }
}
