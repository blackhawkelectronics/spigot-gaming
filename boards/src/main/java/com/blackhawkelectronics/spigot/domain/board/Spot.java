package com.blackhawkelectronics.spigot.domain.board;

public class Spot {
    private final int id;

    public Spot(int spotId) {
        id = spotId;
    }

    public int getId() {
        return id;
    }
}
