package com.blackhawkelectronics.spigot.domain.game;

import com.blackhawkelectronics.spigot.domain.player.GamePlayer;
import com.blackhawkelectronics.spigot.lib.RandomDevice;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.graalvm.polyglot.Value;

import javax.annotation.Nullable;
import java.nio.file.Paths;
import java.util.*;

public class Game {
    private final String gameId;
    private final String niceName;
    private final Script gameScript;
    private final FileConfiguration gameConfig;
    private boolean running = false;
    private final List<GamePlayer> players = new ArrayList<>();
    private final Game parentGame;
    private final Map<String, RandomDevice> randoms = new HashMap<>();

    public Game(String name, String path, FileConfiguration config){
        this(name, path, config, null);
    }

    public Game(String name, String path, FileConfiguration config, @Nullable Game parent) {
        parentGame = parent;
        gameId = name;
        niceName = config.getString("name");

        Bukkit.getLogger().info("Loading game " + name);

        // Config
        gameConfig = config;

        // Script
        gameScript = new Script(Paths.get(path, gameId + ".js"));
        if(gameScript.isLoaded() && gameScript.hasFunction("test")) {
            Bukkit.getLogger().info("Checking for test function");
            Value val = gameScript.runFunction("test");
            if(val != null && val.isString()) Bukkit.getLogger().info("Found test function, which returns: " + val.asString());
            else Bukkit.getLogger().info("Found test function but return was not string");
        }

        // Randoms Init
        //if(gameConfig.contains("random")) {
        //    Object random = gameConfig.get("random");
        //}
    }

    public Value runScript(String name, Object... args) {
        if(gameScript.hasFunction(name)) {
            return gameScript.runFunction(name, args);
        }
        return null;
    }

    // Event Handlers
    public void start() {
        running = true;
    }

    public void end() {
        running = false;
        players.clear();
    }

    public void roll(Player player) {
        //
    }

    public void addPlayer(Player player) {
        players.add(new GamePlayer(player));
    }

    public void removePlayer(Player player) {
        // Find player in list
        Optional<GamePlayer> foundPlayer = players.stream().filter(gamePlayer -> gamePlayer.getPlayer().getName().equals(player.getName())).findFirst();

        // Remove them
        foundPlayer.ifPresent(players::remove);
    }

    // Getters
    public String getNiceName() {
        return niceName;
    }

    public boolean isRunning() {
        return running;
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public boolean hasPlayer(GamePlayer player) {
        return players.stream().anyMatch(gamePlayer -> gamePlayer.equals(player));
    }

    public boolean hasPlayer(Player player) {
        return players.stream().anyMatch(gamePlayer -> gamePlayer.getPlayer().getName().equals(player.getName()));
    }

    public boolean hasPlayer(String playerName) {
        return players.stream().anyMatch(player -> player.getPlayer().getName().equals(playerName));
    }

    // Other stuff
    @Override
    public String toString() {
        return gameId;
    }

    public Object serialize(){
        if(players.isEmpty() && !running){
            return new Object();
        }

    }

    public void deserializeState(Object data) {
        // TODO: Serialize. Question: Should we separate the game state from the logic (like scripts?)
    }
}
