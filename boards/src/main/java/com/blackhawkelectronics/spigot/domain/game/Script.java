package com.blackhawkelectronics.spigot.domain.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import javax.annotation.Nullable;
import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.logging.Logger;

public class Script {
    private final Context context;
    private boolean loaded = false;
    private String language;
    private String[] keys;
    private Logger logger = Bukkit.getLogger();

    // For documentation on the script engine
    // See: https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/package-tree.html

    public Script(Path path) {
        String[] langs = {"js", "python", "ruby"};
        context = Context.newBuilder(langs).allowIO(true).build();

        logger.info("Attempting to load script at " + path.toString());

        final File f = new File(path.toString());
        if(!f.exists()) {
            logger.info(ChatColor.RED + "Script not found");
            return;
        }

        try {
            language = Source.findLanguage(f);
            Source source = Source.newBuilder(language, f).build();
            context.eval(source);
            logger.info("Script evaluated");
            keys = context.getBindings(language).getMemberKeys().toArray(new String[0]);
            loaded = true;
            logger.info("Script load succeed");
        } catch (final Exception exp) {
            logger.warning("Failed to load script. Stack trace:");
            exp.printStackTrace();
        }
    }

    public boolean isLoaded() {
        return loaded;
    }

    public boolean hasFunction(String name) {
        if (!loaded) return false;

        if(Arrays.asList(keys).contains(name)){
            Value item = context.getBindings(language).getMember(name);
            return item.canExecute();
        }
        return false;
    }

    @Nullable
    public Value runFunction(String name, Object... args){
        if (!loaded) return null;

        Value item = context.getBindings(language).getMember(name);
        if(item.canExecute()) {
            return item.execute(args);
        }
        // TODO: Need to have a way to either handle or run java functions (like emitting events, changing players, knowing the next action, etc.)
        return null;
    }

    public boolean hasVariable(String name) {
        if (!loaded) return false;

        if(Arrays.asList(keys).contains(name)){
            Value item = context.getBindings(language).getMember(name);
            return !item.canExecute();
        }
        return false;
    }

    @Nullable
    public Value runVariable(String name){
        if (!loaded) return null;

        Value item = context.getBindings(language).getMember(name);
        if(!item.canExecute()) {
            return item;
        }
        return null;
    }
}
