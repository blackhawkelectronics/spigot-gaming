package com.blackhawkelectronics.spigot.domain.random;

import com.blackhawkelectronics.spigot.lib.RandomDevice;

import java.util.Random;

public class DieSet implements RandomDevice {
    private final int dice;
    private final String[] options;
    private final int upperBound;
    private final Random random;

    public DieSet(int numberOfDice, String[] possibleResults) {
        dice = numberOfDice;
        options = possibleResults;
        upperBound = options.length;
        random = new Random();
    }

    public String getRandom() {
        String result = "";
        int randomNumber;
        for(int i = 0; i < dice; i++) {
            if(i > 0){
                result = result.concat(",");
            }

            randomNumber = random.nextInt(upperBound);
            result = result.concat(options[randomNumber]);
        }
        return result;
    }

    /**
     * Bypasses number of dice
     *
     * @param number how many dice to roll?
     * @return rolled numbers, separated by comma
     */
    public String getRandom(int number) {
        String result = "";
        int randomNumber;
        for(int i = 0; i < number; i++) {
            if(i > 0){
                result = result.concat(",");
            }

            randomNumber = random.nextInt(upperBound);
            result = result.concat(options[randomNumber]);
        }
        return result;
    }
}
