package com.blackhawkelectronics.spigot.domain;

import com.blackhawkelectronics.spigot.domain.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class State {
    /** Game storage */
    private final Map<String, Game> gameNameToGame = new HashMap<>();
    /** Mapping from player ID to game name */
    private final Map<UUID, String> playerToGameName = new HashMap<>();

    public void addGame(Game game) {
        gameNameToGame.put(game.toString(), game);
    }

    public void addPlayerToGame(Player player, String gameName) {
        playerToGameName.put(player.getUniqueId(), gameName);
    }

    public void removePlayerFromGame(Player player) {
        playerToGameName.remove(player.getUniqueId());
    }

    public boolean gameExists(String name) {
        return gameNameToGame.containsKey(name);
    }

    public Game getGame(String name) {
        return gameNameToGame.get(name);
    }

    public Game getGame(Player player) {
        return gameNameToGame.get(playerToGameName.get(player.getUniqueId()));
    }

    public boolean playerInGame(Player player) {
        return playerToGameName.containsKey(player.getUniqueId());
    }

    public Object serialize() {
        Map<String, Object> serializedGames = new HashMap<>(){
            {
                for (Map.Entry<String, Game> entry : gameNameToGame.entrySet()) {
                    put(entry.getKey(), entry.getValue().serialize());
                }
            }
        };

        Map<String, String> serializedPlayerList = new HashMap<>(){
            {
                for(Map.Entry<UUID, String> entry : playerToGameName.entrySet()){
                    put(entry.getKey().toString(), entry.getValue());
                }
            }
        };

        return new HashMap<String, Object>(){
            {
                put("games", serializedGames);
                put("players", serializedPlayerList);
            }
        };
    }

    public static State deserialize(Object data) {

    }

}
