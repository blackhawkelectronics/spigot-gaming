package com.blackhawkelectronics.spigot.domain.random;

import com.blackhawkelectronics.spigot.lib.RandomDevice;
import org.apache.commons.lang.ArrayUtils;

import java.util.Random;

public class CardDeck implements RandomDevice {
    private final String[] cards;
    private String[] unusedCards;
    private final boolean reshuffle;
    private final boolean readd;
    private final Random random;

    /**
     * Create anew Cards Random Device
     *
     * @param possibleResults Array of card names
     * @param reshuffleEmptyStack If stack should be reshuffled once empty
     * @param addBackToStack Add card back to stack once pulled
     */
    public CardDeck(String[] possibleResults, boolean reshuffleEmptyStack, boolean addBackToStack) {
        cards = unusedCards = possibleResults;
        reshuffle = reshuffleEmptyStack;
        readd = addBackToStack;
        random = new Random();
    }

    public String getRandom() {
        int upperBound = unusedCards.length;


        if(upperBound == 0 && reshuffle) {
            unusedCards = cards;
            upperBound = unusedCards.length;
        }

        if(upperBound == 0) {
            return "";
        }

        int randomNumber = random.nextInt(upperBound);

        String selected = unusedCards[randomNumber];

        if(!readd) {
            // Drop Array Element
            unusedCards = (String[]) ArrayUtils.remove(unusedCards, randomNumber);
        }

        return selected;
    }
}
