package com.blackhawkelectronics.spigot.domain.player;

import com.blackhawkelectronics.spigot.domain.pieces.Card;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.GameMode;
import org.bukkit.inventory.Inventory;

import java.util.*;

public final class GamePlayer {
    private Player player;
    private final UUID playerUuid;
    private final PlayerInventory previousInventory;
    private final GameMode previousMode;
    private boolean leftGame = false;
    private final List<Card> cards = new ArrayList<>();
    private int money = 0;
    private int debt = 0;

    public GamePlayer(Player p) {
        player = p;
        playerUuid = player.getUniqueId();
        // Remove inventory from player, saving it
        previousInventory = new PlayerInventory(p.getInventory());
        Inventory newInventory = Bukkit.createInventory(null, 54, "§8Inventory of " + p.getName());
        // Change game mode to adventure
        previousMode = p.getGameMode();
        p.setGameMode(GameMode.ADVENTURE);
    }

    private GamePlayer(UUID playerId, PlayerInventory inventory, GameMode gamemode) {
        playerUuid = playerId;
        player = Bukkit.getPlayer(playerId);
        previousInventory = inventory;
        previousMode = gamemode;
    }

    public void onGameLeave() {
        // Restore inventory
        previousInventory.restore(player.getInventory());
        player.setGameMode(previousMode);

        // Restore GameMode
        leftGame = true;
    }

    protected void finalize() {
        if(!leftGame) {
            onGameLeave();
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public void removeCard(Card card) {
        cards.remove(card);
    }

    public List<Card> getCards() {
        return cards;
    }

    public String[] getCardNames() {
        return (String[]) cards.stream().map(Card::getName).toArray();
    }

    public int getMoney() {
        return money;
    }

    public int setMoney(int newMoney) {
        money = newMoney;
        return money;
    }

    public int addMoney(int addMoney) {
        money += addMoney;
        return money;
    }

    public int getDebt() {
        return debt;
    }

    public int setDebt(int newDebt) {
        debt = newDebt;
        return debt;
    }

    public int addDebt(int addDebt) {
        debt += addDebt;
        return debt;
    }

    public Object serialize() {
        return new HashMap<String, Object>(){
            {
                put("name", player.getName());
                put("uuid", player.getUniqueId());
                put("inventory", previousInventory.serialize());
                put("gamemode", previousMode.toString());
            }
        };
    }

    public static GamePlayer deserialize(Object data) throws Exception {
        if(data instanceof Map) {
            return new GamePlayer(
                    UUID.fromString((String) ((Map<?, ?>) data).get("uuid")),
                    PlayerInventory.deserialize(((Map<?, ?>) data).get("inventory")),
                    GameMode.valueOf((String) ((Map<?, ?>) data).get("gamemode"))
            );
        } else {
            throw new Exception("Bad state given to GamePlayer");
        }
    }
}
