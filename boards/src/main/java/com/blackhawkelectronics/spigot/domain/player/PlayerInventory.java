package com.blackhawkelectronics.spigot.domain.player;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to store a player's inventory.
 */
public class PlayerInventory {
    private ItemStack[] inventoryStacks = new ItemStack[36];
    // private ItemStack[] enderStacks = new ItemStack[27];

    public PlayerInventory(Inventory inventory) {
        ItemStack[] contents = inventory.getContents();
        for(int i = 0; i < contents.length; i++) {
            inventoryStacks[i] = contents[i].clone();
        }
    }

    private PlayerInventory(ItemStack[] stacks){
        inventoryStacks = stacks;
    }

    /**
     * Restore this inventory to the given player inventory
     * @param inventory Player's inventory you wish to restore
     */
    public void restore(Inventory inventory){
        inventory.setContents(inventoryStacks);
    }

    public Map<String, ItemStack> serialize() {
        return new HashMap<String, ItemStack>(){
            {
                for(int i = 0; i < inventoryStacks.length; i++){
                    put(Integer.toString(i), inventoryStacks[i]);
                }
            }
        };
    }

    public static PlayerInventory deserialize(Object data) {
        ItemStack[] stacks = new ItemStack[36];

        if(data instanceof Map) {
            for(int i = 0; i < 36; i++) {
                if(((Map<?, ?>) data).containsKey(Integer.toString(i))) {
                    Object item = ((Map<?, ?>) data).get(Integer.toString(i));
                    if(item instanceof ItemStack) {
                        stacks[i] = (ItemStack) item;
                    } else {
                        stacks[i] = new ItemStack(Material.AIR);
                    }

                } else {
                    stacks[i] = new ItemStack(Material.AIR);
                }
            }
        } else {
            for(int i = 0; i < 36; i++) {
                stacks[i] = new ItemStack(Material.AIR);
            }
        }

        return new PlayerInventory(stacks);
    }

}
