package com.blackhawkelectronics.spigot.application.events.game;

import com.blackhawkelectronics.spigot.domain.game.Game;
import com.blackhawkelectronics.spigot.lib.Event;
import org.bukkit.entity.Player;

public class GameLeave extends Event {
    private final Player player;
    private final String game;
    private final String reason;

    // Constructor for commands
    public GameLeave(Player p, String g) {
        this(p, g, "");
    }

    public GameLeave(Player p, Game g, String leftReason){
        this(p, g.toString(), leftReason);
    }

    public GameLeave(Player p, String g, String leftReason) {
        player = p;
        game = g;
        reason = leftReason;
    }

    public Player getPlayer() {
        return player;
    }

    public String getGameName() {
        return game;
    }

    public String getReason() {
        return reason;
    }
}
