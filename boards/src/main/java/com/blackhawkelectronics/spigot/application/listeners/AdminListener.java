package com.blackhawkelectronics.spigot.application.listeners;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class AdminListener implements Listener {
    private FileConfiguration config;

    /**
     *
     * @param c Configuration
     */
    public AdminListener(FileConfiguration c){
        config = c;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // If first time, need to setup user.

        if (config.getBoolean("enabled") && player.getWorld().getName().equalsIgnoreCase(config.getString("world"))) {
            player.spigot().sendMessage(new ComponentBuilder().color(ChatColor.YELLOW).append("Welcome " + player.getName() + " to the gaming world!").create());
        }
    }

    @EventHandler
    public void onPlayerWorldChange(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        if (config.getBoolean("enabled") && player.getWorld().getName().equalsIgnoreCase(config.getString("world"))) {
            player.spigot().sendMessage(new ComponentBuilder().color(ChatColor.YELLOW).append("Welcome " + player.getName() + " to the gaming world!").create());
        }
    }
}
