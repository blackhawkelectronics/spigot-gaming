package com.blackhawkelectronics.spigot.application.events.game;

import com.blackhawkelectronics.spigot.lib.Event;
import org.bukkit.entity.Player;

public class GameJoin extends Event {
    private Player player;
    private String game;

    // Constructor for commands
    public GameJoin(Player p, String g) {
        player = p;
        game = g;
    }

    public Player getPlayer() {
        return player;
    }

    public String getGameName() {
        return game;
    }
}
