package com.blackhawkelectronics.spigot.application.events.game;

import com.blackhawkelectronics.spigot.lib.Event;
import com.blackhawkelectronics.spigot.domain.game.Game;
import org.bukkit.entity.Player;

public class GameRoll extends Event {
    private final Player player; // Player who started the game
    private final String game;

    // Constructor for commands
    public GameRoll(Player p, String gameName){
        game = gameName;
        player = p;
    }

    public String getGameName() {
        return game;
    }

    public Player getPlayer() {
        return player;
    }
}
