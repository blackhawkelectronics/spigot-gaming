package com.blackhawkelectronics.spigot.application.listeners;

import com.blackhawkelectronics.spigot.application.events.game.*;
import com.blackhawkelectronics.spigot.domain.State;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import javax.annotation.Nullable;
import java.util.logging.Level;

public class GameListener implements Listener {
    private final State gameState;

    public GameListener(State mapper){
        gameState = mapper;
    }

    @EventHandler
    public void onGameStart(GameStart event) {
        if(checkGameExists(event.getGameName(), event.getPlayer())) {
            gameState.getGame(event.getGameName()).start();
            Bukkit.getLogger().info("Game started for " + event.getGameName());
        }
    }

    @EventHandler
    public void onGameEnd(GameEnd event) {
        if(checkGameExists(event.getGameName(), event.getPlayer())) {
            gameState.getGame(event.getGameName()).end();
            Bukkit.getLogger().info("Game ended for " + event.getGameName());
        }
    }

    @EventHandler
    public void onGameJoin(GameJoin event) {
        if(checkGameExists(event.getGameName(), event.getPlayer())) {
            gameState.getGame(event.getGameName()).addPlayer(event.getPlayer());
        }
    }

    @EventHandler
    public void onGameLeave(GameLeave event) {
        if(checkGameExists(event.getGameName(), event.getPlayer())) {
            gameState.getGame(event.getGameName()).removePlayer(event.getPlayer());
        }
    }

    @EventHandler
    public void onGameRoll(GameRoll event) {
        if(checkGameExists(event.getGameName(), event.getPlayer())) {
            gameState.getGame(event.getGameName()).roll(event.getPlayer());
        }
    }



    private boolean checkGameExists(String name, @Nullable Player player) {
        if(gameState.gameExists(name)) return true;

        if (player != null) {
            player.sendMessage(name + " does not exist");
        } else {
            Bukkit.getLogger().log(Level.SEVERE, "Game " + name + " did not exist when auto-initiating game event.");
        }

        return false;
    }
}
