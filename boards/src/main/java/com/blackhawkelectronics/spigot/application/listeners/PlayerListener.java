package com.blackhawkelectronics.spigot.application.listeners;

import com.blackhawkelectronics.spigot.application.events.game.GameLeave;
import com.blackhawkelectronics.spigot.domain.State;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

public class PlayerListener implements Listener {
    private final State gameState;

    public PlayerListener(State state){
        gameState = state;
    }

    @EventHandler
    public void onPlayerModeChange(PlayerGameModeChangeEvent event){
        // If player switches out of game mode
        Player player = event.getPlayer();
        if(gameState.playerInGame(player) && !event.getNewGameMode().equals(GameMode.ADVENTURE)) {
            if(player.isOp() || player.hasPermission("boards.admin")) {
                player.sendMessage("Warning: Playing a game while not in Adventure is considered cheating.");
            } else {
                Bukkit.getPluginManager().callEvent(new GameLeave(player, gameState.getGame(player), "kicked:mode"));
            }
        }
    }

    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent event){
        Player player = event.getPlayer();
        Bukkit.getPluginManager().callEvent(new GameLeave(player, gameState.getGame(player), "world"));
    }
}
