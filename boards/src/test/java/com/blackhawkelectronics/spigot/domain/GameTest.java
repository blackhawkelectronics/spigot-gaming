package com.blackhawkelectronics.spigot.domain;

import com.blackhawkelectronics.spigot.domain.game.Game;
import org.bukkit.configuration.file.FileConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit test for simple App.
 */
@ExtendWith(MockitoExtension.class)
public class GameTest
{
    @Mock
    FileConfiguration config;


    @Test
    public void testNormalCreate() {
        Mockito.when(config.getString("name")).thenReturn("My Game");

        Game game = new Game("myGame", "path", config);

        Assertions.assertEquals("My Game", game.getNiceName());
        Assertions.assertEquals("myGame", game.toString());
    }
}
