# Main Configuration File

All config files are stored in the `plugins/Boards` folder on the server.


## Options
The following are the different options in the primary config file `config.yml`:

### World

This plugin currently only supports a single game world.
This option specifies the name of that world.
It is caps sensitive.

### Enabled

I think you can guess this one - if the plugin is enabled on the server.

### Games

An array of game names.

These will refer to [individual game files](gameConfig.md).

## Example

```yml
world: MyGameWorld
enabled: true
games:
  - clue
  - monopoly
  - life
```