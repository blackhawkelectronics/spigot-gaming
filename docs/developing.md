# Developing

This document is for those who wish to develop this plugin.
If you are looking for how to develop the game scripts, go [here](gameScripts.md).

## Structure

The structure of this plugin is loosely following the DDD (Domain-driven development) pattern.
Therefore, you have the following parts:

- **Boards**: The application itself. This can't be moved - otherwise it would be under infrastructure.
  It is responsible for initializing all the necessary parts of the code.
- **Domain**: The core of the plugin. Players should never be able to touch this directly!
  It is supposed to be for domain logic and classes that hold core data.
- **Application**: Events and their listeners go here. It bridges between the Domain and Infrastructure layers.
- **Infrastructure**: Anything that acts with the outside layer. 
  In this case, instead of being http, it is commands primarily.
  In the future, there may be a database here.
  Sadly, there are no service providers.

If you noticed, these are like layers of a ball.
The domain layer is the core, the application layer goes around that, and the infrastructure is the crust.
Others have called this hexagonal architecture.

You can also see our [planning page](planning.md) where we figure out how to abstract games.
