
# Welcome to Boards for Minecraft!

If you want to help develop, see the [developing page](developing.md).

If you are interested in developing a game with this platform see the following:

  - [Boards config file](mainConfig.md)
  - [Game Configuration Files](gameConfig.md)
  - [Game Scripts](gameScripts.md)