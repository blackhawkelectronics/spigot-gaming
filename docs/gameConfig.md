# Game Config File

For each game, there is a yaml file that controls the basic mechanics of the game.

The name of this file should be the no-spaces name listed in the [main config file](mainConfig.md) plus `.yml`.

## Mechanics Config

### Name

Each game must have a nice-looking name for the game (instead of the ugly no-spaces one used for this file).

### Turn Based

**Key:** mechanics.turnbased  
**Type:** *Boolean*  
**Default:** true

### Movement Style

**Key:** options.movement  
**Type:** Enumerated *String*

Possible options for this are:
  - _none_ (or missing)
  - _linear_ - move along a line (1D)
  - _grid_ - simple x/y coordinate movement (2D)
  - _area_ - define which areas you can go to
  - _3d_ - like grid, just in x, y *and* z

### Looping (linear movement style)

**Key:** mechanics.loop  
**Type:** *Boolean*

## Board

This section has the actual definition for the board.
Usually it will be the longest section, since each spot must be listed and configured

### Spots

**Key:** board.spots  
**Type:** *Array* of *Objects*

This is an array of objects that lists all the spots in a game.
The movement style must not be empty or none.

The object keys are optional unless specified.

**Spot Keys for Linear/Looping Movement**

  - **id**: Unique string for reference purposes
  - **x**: Spot number (Required)
    - Starts at 0.
    - Sequential: You cannot skip numbers (unless you use nextSpot).
    - If in a branch, the starting number is arbitrary, but must be sequential.
  - **nextBranch**: This spot splits into branches (array) or changes (integer). First spot is always 0.
  - **nextSpot**: x coordinate of next spot.
  - **onWalk**: script function to run when you walk over this spot

### Branches

**Key:** board.branches
**Type:** *Array* of *Objects*

Only for the linear or loop movement styles that use decision branches.

Gives some specifics for branches.

All keys below are optional, except ID

**Keys**

  - **id**: Branch number
  - **name**: Name of the branch
  - **spots**: See Spots

### Turn Stages

Many games have stages in each turn (like buy/sell, move, attack).
This key makes that easy by providing an enum to iterate through.
The strings given in this array will be used with the onTurnStep() function.

**Key:** turnstages  
**Type:** *Array* of *Strings*

## Random Devices

A board game usually has some element of randomness, whether that be dice, a spinner, or cards.
This section defines those random elements as Devices.

**Key:** random  
**Type:** _Array_ of _Objects_

The objects are defined with the following keys:

### Name

This is an identifier that you will use in scripts.
It must be unique.

**Key:** type  
**Type:** String Enum  
**Default:** The type in lowercase (remember it must be unique)

### Type

**Key:** type  
**Type:** String Enum  
**Options:** Dice, Cards

Dice is a random integer generator, so can be used for spinners as well.  
Cards represent **decks** of cards that are randomly selected.

### Number of Dice

This is for dice only.

**Key:** number  
**Type:** integer  
**Default:** 1

Determines how many dice are rolled when you need the result of individual dice.
If you only need a single value, you don't need to use multiple.
If you are not using a script, you cannot use this option.

### Cards

**Key:** type  
**Type:** Object

Keys in cards:
 - **name**: Required. Nice name for cards that may be shown to the player.
 - **value**: Optional value used by the game script. Passed as a string.
 - **count**: How many copies of this card are there in the deck
 - **holdable**: Can be held by the player (Default: false)
 - **private**: Card draw should not be broadcasted to players. Default is false.

Example for Candy Land:
```yaml
name: Single Green
value: green
```

### Return to Deck

After each card draw, do you want the c

**Key:** reshuffle
**Type:** boolean

## Setup

This section deals with setting up a game

### Player

Players often start with certain resources. This section will define that.

#### Cards

Players are given a specific set of cards (like resource cards)

**Key:** setup.player.cards  
**Type:** array of of objects

Keys (All Required)
- deck: Name of card deck (from random.#.name)
- name: Name of card in deck (from random.#.cards)
- count: number of cards

#### Random Cards

Pull a certain number of cards out of a deck

**Key:** setup.player.randcards  
**Type:** array of objects

Keys (All Required)
  - deck: Name of card deck
  - count: number of cards

### Money

Don't worry about counting out denomonations of bills.
Just put a number here and your players will start off with that much!

**Key:** setup.player.money  
**Type:** integer

### Debt

There's a few sad games that start you off with debt (shame on them).

**Key:** setup.player.debt  
**Type:** integer

## Finish

THis section deals with basic winning and loosing conditions