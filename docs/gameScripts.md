## Game Scripts

Game scripts are Javascript files that contain several functions that represent events in a game.
These modify the standard running of a game outside the normal

## Methods

### onStart

### onEnd

Called at end of game

### onSpotWalk

Modify what happens as you walk across ths pot.
For linear movement style only.

Parameters:
  - Integer x
  - Integer branch

## Method Return

Every method should return an object.
This object will modify how the game works.

### Game Message

> Player has rolled a 2

### World Message

> Player has won a game!
