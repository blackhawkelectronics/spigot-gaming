# Planning

## Abstracted

These are things that go between all the games so we don't have to program for them specifically.

    - Each game has variables for each user - only the users in the game. May use book.
    - Spectators can get stats (press button?).
    - Spectator areas
    - Game board Config (With wizard)
    - Next Spot indicator
    - Roller (with config) - either item or command
    - Know which spot each user is supposed to be on and watch it. When they walk on it, it activates the action.
    - Watch redstone blocks with BlockRedstoneEvent
    - Turn commands
    - Card selector
    - Global CPI? - will need cpi for each game to scale up.

### Scripting
 
Scripting will be needed for various events. The language that will be used is Javascript.
Each event will call a predetermined function in the script.
Each game will have it's own script.
    
Scripts Include:
    - Pre-game trigger: Run when the first non-spectator user enters the game
    - New Spectator: Run when a player enters the game as a spectator.
    - Spectator Left: Run when a player exits the game
    - Game Start: Runs when the game starts, of course
    - Game Check: Runs at various points, depending on config. Generally after every turn. Primary purpose is to check if the game has been won.
    - Roll: Run when the player rolls one dice
    - Double Roll: Run when two dice are rolled.
    - Spot Highlighted: Run when a spot is highlighted. Useful for determining last
    - Spot Landed: Run when a watched (highlighted) spot is stepped on.
    - Spot Activate: Run when a spot is activated


### Spots

Each game has spots.

General Spot configuration for a game:

    - Highlight type (See [Particle List](https://www.spigotmc.org/wiki/particle-list-1-8-8/))
    - Roll Type
  
### Game State

The game state is defined in the game's config.
The definition is fairly simple, just a key and the variable type as the value.

## Games

### Life
    - Rules: https://boardgamemanuals.fandom.com/wiki/The_Game_of_Life
    - Booleans: Stock, Life, House, Fire, Auto, Married, Doctor?
    - Int: Money, Boy, Girl, Salary
    - String: Occupation
    - Roll: 1-10
    - Has Mandatory action, stop, and optional action, and split spots - so will have to search for these each roll
    - Share the wealth card received when pay day is landed on exactly

### Monopoly
    - Array of each spot:
      - purchase price
      - Color category
      - Owner
      - Level (-1 = sale, 0 = mortgage, 1 = owned, 2+ = houses/hotel
      - Railroads count on how many railroads you have
      - rent for each positive level
    - Integer: Money
    - Roll: 2-12 with double probability for evens
    - Ending options: Bankrupt all or upon command (endgame or quit/yield)


### Clue
    - Must have all players. Unavailable players will be replaced with NPC's
    - May not track spots
    - Rooms
    - Players get teleported upon an suggestion or accusation

### Falls and Ladders
    - Rules: https://boardgamemanuals.fandom.com/wiki/Snakes_and_ladders
    - Roll: 1-6
    - Simple addition with actions for some spaces.

### Candy land
    - Rules: https://boardgamemanuals.fandom.com/wiki/Candy_Land
    - Roll: Random card with color or double color.


## Questions
  - Can I do color in chat? Yes, with the 
  - Can I do links in chat for commands?
  - Can I watch buttons and plates?
